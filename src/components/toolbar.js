import React from 'react';


class Toolbar extends React.Component {
    constructor(props){
        super();
        this.data=props.data;
    }

onSelected = ({target}) => {
    this.props.changeFilter(target.value);
}

    render(){
        return(
            <div className='toolbar'>
                <select onChange={this.changeFilter}>
                    {this.data.map(
                        (tbitem, index) => <option key={index} value={index}>{tbitem.name}</option>
                    )}
                </select>
                <input label="soundboard" value={this.props.filterValue} onChange={({target}) => this.props.setFilterText(target.value)}>
                </input>
            </div>
            );
    }
}

export default Toolbar;