import React from 'react';
import Item from './item/item';


class Soundboard extends React.Component{
       
    render(){
        
        return(
            <div  className ="soundboard">
                    {this.props.filteredItems.map(
                    (item) => <Item item={item} key={item.name}/>
                    )}
            </div>
        );
    }
}

export default Soundboard;