import bugsSound from "../assets/sounds/looney/bugsSound.mp3";
import daffySound from "../assets/sounds/looney/daffySound.mp3";
import porkySound from "../assets/sounds/looney/porkySound.mp3";
import roadSound from "../assets/sounds/looney/roadSound.mp3";
import speedySound from "../assets/sounds/looney/speedySound.mp3";
import tazSound from "../assets/sounds/looney/tazSound.mp3";
import tweetySound from "../assets/sounds/looney/tweetySound.mp3";
import yoseSound from "../assets/sounds/looney/yoseSound.mp3";
import bugsPic from "../assets/images/looney/bugsPic.jpg";
import daffyPic from "../assets/images/looney/daffyPic.jpg";
import porkyPic from "../assets/images/looney/porkyPic.jpg";
import roadPic from "../assets/images/looney/roadPic.png";
import speedyPic from "../assets/images/looney/speedyPic.jpg";
import tazPic from "../assets/images/looney/tazPic.jpg";
import tweetyPic from "../assets/images/looney/tweetyPic.jpg";
import yosePic from "../assets/images/looney/yosePic.jpg";
import homerPic from "../assets/images/simpsons/homer.png";
import bartPic from "../assets/images/simpsons/bart.jpg";
import lisaPic from "../assets/images/simpsons/lisa.jpg";
import flandersPic from "../assets/images/simpsons/flanders.jpg";
import burnsPic from "../assets/images/simpsons/mrburns.jpg";
import homerSound from "../assets/sounds/simpsons/homer.wav";
import lisaSound from "../assets/sounds/simpsons/lisa.wav";
import bartSound from "../assets/sounds/simpsons/bart.wav";
import flandersSound from "../assets/sounds/simpsons/flanders.wav";
import burnsSound from "../assets/sounds/simpsons/mrburns.wav";

const data = [{
    name: "Looney Tunes",
    items:[{
    name: "Bugs Bunny",
    img: bugsPic,
    sound: bugsSound
},{
    name: "Daffy Duck",
    img: daffyPic,
    sound: daffySound
},{
    name: "Tazmanian Devil",
    img: tazPic,
    sound: tazSound
},{
    name: "Roadrunner",
    img: roadPic,
    sound: roadSound
},{
    name: "Speedy Gonzalez",
    img: speedyPic,
    sound: speedySound
},{
    name: "Porky Pig",
    img: porkyPic,
    sound: porkySound
},{
    name: "Tweety",
    img: tweetyPic,
    sound: tweetySound
},{
    name: "Yosemite Sam",
    img: yosePic,
    sound: yoseSound
}]},
{
name: "The Simpsons",
items: [{
    name: "Homer",
    img: homerPic,
    sound: homerSound
},{
    name: "Lisa",
    img: lisaPic,
    sound: lisaSound
}
,{
    name: "Bart",
    img: bartPic,
    sound: bartSound
},{
    name: "Flanders",
    img: flandersPic,
    sound: flandersSound
},{
    name: "Mr Burns",
    img: burnsPic,
    sound: burnsSound
}]}]

export default data;