import React from 'react';
import logo from './logo.svg';
import './App.css';
import Toolbar from './components/toolbar';
import Soundboard from './components/soundboard'
import data from './data/data';

class App extends React.Component {
  constructor(){
    super();
    this.state = { selected: 0,
    filterText: '' }
    }

  changeFilter = (sbIndex) => {
    this.setState({selected : sbIndex })
  }

  setFilterText = (filterText) => {
  this.setState({filterText});
  }

  render(){
     console.log('hier zijn we');
     const filteredItems = data[this.state.selected].items.filter(
       (item) => item.name.includes(this.state.filterText)
     );

    return (
    <div className="App">
      <header className="App-header">Soundboard</header>
      <section>
        <Toolbar data={data} changeFilter={this.changeFilter} setFilterText = {this.setFilterText} filterValue={this.state.filterText} />
      </section>
      <section>
         <Soundboard filteredItems = {filteredItems} data={data} state={this.state.selected} />
      </section>
      <footer className="footer">@Katrien Raats</footer>
    </div>
  )};
}

export default App;
